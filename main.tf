resource "aws_instance" "web" {
  ami         = "ami-076309742d466ad69"  
instance_type = "t2.micro"

  tags = {
    Name = "HelloWorld"
  }
}

terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.45.0"
    }
  }
}

provider "aws" {
region = "eu-central-1"
}
